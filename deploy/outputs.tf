output "db_host" {
  value = aws_db_instance.main.address
}

output "bastion_host" {
  value = aws_instance.bastion.public_dns
}

# Publicly accessible piece of our api.
# Load balancer hides ecs tasks in private subnet
output "api_endpoint" {
  value = aws_lb.api.dns_name
}