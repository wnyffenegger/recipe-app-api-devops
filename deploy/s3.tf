# Force destroy allows us to destroy the bucket with Terraform
# but that may not be desirable in a real deployment
resource "aws_s3_bucket" "app_public_files" {
  bucket = "${local.prefix}-files"

  acl           = "public-read"
  force_destroy = true
}

