# Some resources only support really short names
variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "william.nyffenegger@gmail.com"
}

variable "db_username" {
  description = "username for RDS postgres instance"
}

variable "db_password" {
  description = "password for the RDS postgres instance"
}

# Matches name of key pair in ec2 console
variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

# Set images of containers
# default to latest for testing locally
variable "ecr_image_api" {
  description = "ecr image for api"
  default     = "732539517888.dkr.ecr.us-east-2.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ecr image for proxy"
  default     = "732539517888.dkr.ecr.us-east-2.amazonaws.com/recipe-app-api-proxy:latest"
}

# Set via vars in Gitlab or via tfvars for testing locally
variable "django_secret_key" {
  description = "secret key for django app"
}