terraform {
  backend "s3" {
    bucket         = "wnyffenegger-recipe-app-tform"
    key            = "recipe-app.tfstate"
    region         = "us-east-2"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-east-2"
  version = "~>2.54.0"
}

# Locals are local variables to this terraform module
# that you can apply throughout the module.
# Notice how this local overrides the vars file
locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

# Current region as a variable that we can use
data "aws_region" "current" {}