# Get the id of the most recent ami programmatically
# ex. ami-089c6f2e3866f0f14

data "aws_ami" "amazon_linux" {
  # Make sure image was created by amazon and you aren't getting spoofed
  most_recent = true
  owners      = ["amazon"]

  # Filter images by image name
  # Filter by regex where name begins and ends with a standard string
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"]
  }
}

# Role allowing ec2 instance to assume other roles
# in our AWS account
resource "aws_iam_role" "bastion" {
  name               = "${local.prefix}-bastion"
  assume_role_policy = file("./templates/bastion/instance-profile-policy.json")

  tags = local.common_tags
}

# Attach a policy that we want to allow the EC2
# instance to be able to assume. We could do this with as many
# policies as desired
resource "aws_iam_role_policy_attachment" "bastion_attach_policy" {
  role       = aws_iam_role.bastion.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

# Create a profile for the role to apply to bastion
# this profile could be applied to any number of instances
resource "aws_iam_instance_profile" "bastion" {
  name = "${local.prefix}-bastion-instance-profile"
  role = aws_iam_role.bastion.name
}

resource "aws_instance" "bastion" {
  ami           = data.aws_ami.amazon_linux.id
  instance_type = "t2.micro"

  # Configure ec2 instance on startup
  # Assign instance profile with assume role permissions
  # to bastion
  # Set key name so that we can ssh and then set it to be public
  # so that we can connect.
  user_data            = file("./templates/bastion/user.sh")
  iam_instance_profile = aws_iam_instance_profile.bastion.name
  key_name             = var.bastion_key_name
  subnet_id            = aws_subnet.public_a.id

  vpc_security_group_ids = [
    aws_security_group.bastion.id
  ]

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-bastion")
  )
}

# Allow access from the internet over ssh
# Allow bastion to access http/https over internet
# But also allow postgres access from bastion
resource "aws_security_group" "bastion" {
  name        = "${local.prefix}-bastion"
  description = "Control bastion inbound and outbound access"
  vpc_id      = aws_vpc.main.id

  ingress {
    description = "SSH from internet"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # This only works if RDS allows ingress from the bastion server
  egress {
    from_port = 5432
    to_port   = 5432
    protocol  = "tcp"
    cidr_blocks = [
      aws_subnet.private_a.cidr_block,
      aws_subnet.private_b.cidr_block
    ]
  }

  tags = local.common_tags
}