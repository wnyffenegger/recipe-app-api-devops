#!/bin/bash

# Install docker when bastion first starts
# this is like an Ansible script running after
# the instance is created

sudo yum update -y

# Install and setup docker via systemctl
sudo amazon-linux-extras install -y docker
sudo systemctl enable docker.service
sudo systemctl start docker.service

# Give ec2-user permissions to interact with Docker
sudo usermod -aG docker ec2-user